<?php

namespace App\Http\Controllers;

use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Post::orderBy('id', 'DESC')->paginate();
    }
    public function show($slug)
    {
        return Post::where('slug', $slug)->first();
    }
}
